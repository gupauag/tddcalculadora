package br.com.calculadora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalucladoraTeste {

    private Calculadora calculadora;

    @BeforeEach //antes de executar cada metodo ele cria uma calculadora. não preciso então criar o objeto sempre!
    public void setUp(){
        calculadora = new Calculadora();
    }

    @Test
    public void testaASomaDoisNumerosInteiros(){

        int resultado = calculadora.soma(1,2);

        Assertions.assertEquals(3, resultado); // ele espera que o resultado seja igual a 3!
    }

    @Test
    public void testaASomaDeDoisNumerosFlutuantes(){

        double resultado = calculadora.soma(2.3, 3.4);
        Assertions.assertEquals(5.7, resultado);
    }

    @Test
    public void testaDivisaoDoisNumerosInteiros(){
        int resultado = calculadora.divisao(10,3);
        Assertions.assertEquals(3,resultado);
    }

    @Test
    public void testaDivisaoDoisNumerosNegativos(){
        int resultado = calculadora.divisao(-10,-2);
        Assertions.assertEquals(-5,resultado);
    }

    @Test
    public void testaDividaoNumFlutuantes(){
        Double resultado = calculadora.divisao(10.5, 2.5);
        Assertions.assertEquals(4.2,resultado);
    }

    @Test
    public void testaMultipicacaoNumInteiros(){
        int result = calculadora.multiplicacao(2,5);
        Assertions.assertEquals(10,result);
    }

    @Test
    public void testaMultiplicacaoNumNegatigos(){
        int result = calculadora.multiplicacao(-2,-5);
        Assertions.assertEquals(-10,result);
    }

    @Test
    public void testaMutiplicacoNumFlutuantes(){
        double result = calculadora.multiplicacao(2.5, 3.5);
        Assertions.assertEquals(8.75, result);
    }

    @Test
    public void testaCaminhoTristeSoma(){
        Assertions.assertThrows(RuntimeException.class,
                () -> {
                    calculadora.soma(-2, 2);
                });
    }

}
