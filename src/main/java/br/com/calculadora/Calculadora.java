package br.com.calculadora;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Calculadora {

    public Calculadora(){

    }

    public int soma(int primeiroNumero, int segundoNumero) {
        if(primeiroNumero < 0 || segundoNumero < 0)
            throw new RuntimeException("Os numeros não podem ser negativos");

        return primeiroNumero+segundoNumero;
    }

    public double soma (double p, double s){
        Double resultado = p + s;
        BigDecimal bigDecimal = new BigDecimal(resultado).setScale(3, RoundingMode.HALF_EVEN);

        return bigDecimal.doubleValue();
    }

    public int divisao(int valor1, int valor2) {
        return valor1/valor2;
    }

    public double divisao(double valor1, double valor2) {
        Double resultado = valor1 / valor2;
        return resultado;
    }

    public int multiplicacao(int valo1, int valor2) {
        int result = valo1 * valor2;
        return result;
    }

    public double multiplicacao(double valor1, double valor2) {
        Double resultado = valor1 * valor2;
        return resultado;
    }
}
